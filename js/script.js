function User(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.getLogin = function() {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
    this.setFirstName = (newName) => {

        Object.defineProperty(this, 'firstName', {
            writable: true
        });

        this.firstName = newName;

        Object.defineProperty(this, 'firstName', {
            writable: false
        });
    };

    this.setLastName = (newLastName) => {

        Object.defineProperty (this, 'lastName', {
            writable: true
        });

        this.lastName = newLastName;

        Object.defineProperty (this, 'lastName', {
            writable: false
        });
    };

}

let user = new User(prompt("Please enter your first name", "Irina"), prompt("Please enter your last name", "Kavun"));

user.setFirstName('Masha');
user.setLastName('Tralala');

user.firstName = 'Tim';
console.log(user.firstName);
user.lastName = 'Haidai';
console.log(user.lastName);

console.log(user.getLogin());



